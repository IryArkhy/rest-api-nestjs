import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import UsersModule from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import config from '../config';


@Module({
    imports: [UsersModule, PassportModule.register({ defaultStrategy: 'jwt' }), JwtModule.register({
        secret: config.secretJwtKey,
        signOptions: { expiresIn: '600s' },
    }),],
    controllers: [AuthController],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [UsersModule, AuthService]
})
export default class AuthModule { }

