import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import config from '../../config';
import { AuthService } from '../auth.service';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.secretJwtKey,
        });
    }

    async validate(payload: JwtPayload, done: VerifiedCallback) {
        const userId = await this.authService.validateUser(payload.id);
        if (!userId) {
            return done(new UnauthorizedException(), false);
        }
        done(null, userId);
    }
}
