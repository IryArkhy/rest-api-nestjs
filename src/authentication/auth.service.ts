import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import CreateUserDto from '../users/dto/create-user.dto';
import { UserService } from '../users/users.service';
import { UserAfterLogin, PublicUserInfo } from '../users/interfaces/user.interface';
import { AuthErrMsg } from './enums/auth.enum';


@Injectable()
export class AuthService {

    constructor(private userService: UserService) { }

    async validateUser(id: string): Promise<any> {
        return await this.userService.getUserById(id);
    }

    async register(user: CreateUserDto): Promise<UserAfterLogin> {
        const { email, password } = user;
        if (!email || !password) throw new HttpException(AuthErrMsg.REQUIRED_FIELDS, HttpStatus.BAD_REQUEST);

        const existingUser = await this.userService.checkExistingUser(email);
        if (existingUser) throw new HttpException(AuthErrMsg.DUPLICATED_EMAIL, HttpStatus.CONFLICT);

        const newUser = await this.userService.createUser(user);
        return newUser;
    }

    async login(user: { email: string, password: string }): Promise<UserAfterLogin> {
        const { email, password } = user;

        const isValidPassword = await this.userService.validatePassword(email, password);
        if (!isValidPassword) throw new HttpException(AuthErrMsg.INVALID_FIELDS, HttpStatus.BAD_REQUEST);

        await this.userService.generateNewToken(email);
        const updatedUser = await this.userService.checkExistingUser(email);
        return updatedUser;
    }

    async current(user: PublicUserInfo): Promise<UserAfterLogin> {
        const userFromDB = await this.userService.checkExistingUser(user.email)
        return userFromDB;
    }
}
