export enum AuthErrMsg {
    DUPLICATED_EMAIL = "Email in use",
    REQUIRED_FIELDS = "Missing required fields",
    INVALID_FIELDS = "Invalid email or password.",
}
