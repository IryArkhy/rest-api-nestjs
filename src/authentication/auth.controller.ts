import { Controller, Get, Post, Body, UseGuards, Request, HttpException, HttpStatus, HttpCode } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import CreateUserDto from '../users/dto/create-user.dto';
import { UserAfterLogin } from '../users/interfaces/user.interface';
import { AuthErrMsg } from './enums/auth.enum';


@Controller('api/auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('register')
    registerUser(@Body() user: CreateUserDto): Promise<UserAfterLogin> {
        return this.authService.register(user);
    }

    @Post('login')
    @HttpCode(200)
    loginUser(@Request() req): Promise<UserAfterLogin> {
        if (!req.body.email || !req.body.password) throw new HttpException(AuthErrMsg.REQUIRED_FIELDS, HttpStatus.BAD_REQUEST);
        return this.authService.login(req.body);
    }

    @UseGuards(JwtAuthGuard)
    @Get('current')
    getPreviouslyLogedIn(@Request() req): Promise<UserAfterLogin> {
        return this.authService.current(req.user);
    }

}