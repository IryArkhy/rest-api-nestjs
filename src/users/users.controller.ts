import { Controller, Get, Patch, Delete, Body, Param, Query, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { UserInfoInArrs, PublicUserInfo, UserAfterLogin, RespondObject } from './interfaces/user.interface';
import { UserService } from './users.service';
import { UserErrMsg } from './enums/msg.enum'
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';

@Controller('api/users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Get('all')
    getAllUsers(): Promise<UserInfoInArrs[]> {
        return this.userService.getAllUsers();
    }

    @Get()
    sortUserBySubscr(@Query('sub') sub: string): Promise<UserInfoInArrs[]> {
        return this.userService.sortUserBySubscr(sub);
    }


    @Get(':userId')
    getUserById(@Param('userId') userId: string): Promise<PublicUserInfo> {
        return this.userService.getUserById(userId);
    }

    @Get('mail/:email')
    findByEmail(@Param('email') email: string): Promise<PublicUserInfo> {
        return this.userService.findByEmail(email);
    }

    @Patch(':userId')
    editUser(@Param('userId') userId: string, @Body() newFields: { email?: string, subscription?: string }): UserAfterLogin {
        if (!newFields.email && !newFields.subscription) throw new HttpException(UserErrMsg.NO_UPDATE_FIELDS, HttpStatus.BAD_REQUEST);
        return this.userService.editUser(userId, { ...newFields })
    }

    @UseGuards(JwtAuthGuard)
    @Patch('pass/:userId')
    changePassword(
        @Param('userId') userId: string,
        @Body('email') email: string,
        @Body('oldPassword') oldPassword: string,
        @Body('newPassword') newPassword: string
    ): Promise<RespondObject> {
        if (!oldPassword) throw new HttpException(UserErrMsg.NO_OLD_PASS, HttpStatus.BAD_REQUEST);
        if (!newPassword) throw new HttpException(UserErrMsg.NO_NEW_PASS, HttpStatus.BAD_REQUEST);
        return this.userService.changePassword(userId, email, oldPassword, newPassword);
    }

    @Delete(':userId')
    deleteUser(@Param('userId') usertId: string): Promise<RespondObject> {
        return this.userService.deleteUser(usertId);
    }
}