export enum UserErrMsg {
    SERVICE_UNAVAILABLE = "Something wrong with DB.",
    NOT_FOUND = "The user is not found.",
    INVALID_ID = "Bad request. Invalid ID.",
    WRONG_OLD_PASSWORD = "Wrong old password.",
    NO_OLD_PASS = "No old password provided",
    NO_NEW_PASS = "No new password provided",
    NO_UPDATE_FIELDS = "You can update only subscription and email. No fields for the update provided."
}

export enum UserSccssMsg {
    CHANGED_PASSWORD = "Passwod successfuly changed",
    DELETED = "Deleted successfully"
}