import { Document } from 'mongoose';

export interface User extends Document {
    _id: string;
    email: string;
    password: string;
    subscription: string;
    contacts: Array<{}>;
    token: string;
    createdAt: Date;
    updatedAt: Date;
    __v: number
}

export interface PublicUserInfo {
    _id: string;
    email: string;
    subscription: string;
}

export interface UserInfoInArrs {
    email: string;
    subscription: string;
}

export interface UserAfterLogin {
    user: {
        _id: string;
        email: string;
        subscription: string;
        contacts: Array<{}>;
        createdAt: Date;
    };
    token: string
}

export interface RespondObject {
    message: string;
    user: UserAfterLogin;
}