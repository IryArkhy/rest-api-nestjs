import * as mongoose from "mongoose";
import * as jwt from "jsonwebtoken";
import * as bcrypt from "bcryptjs";
import config from "../../config";
import { UserAfterLogin, PublicUserInfo } from "../interfaces/user.interface";


export const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        indexes: true,
        unique: true,
        lowercase: true,
        trim: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    subscription: {
        type: String,
        enum: ["free", "pro", "premium"],
        default: "free"
    },
    contacts: [
        {
            type: mongoose.Schema.ObjectId,
            ref: "Contacts"
        }
    ],
    token: {
        type: String,
        default: null
    },
}, { timestamps: true });

UserSchema.methods.getPublicFields = function (): UserAfterLogin {
    const returnObject = {
        user: {
            _id: this._id,
            email: this.email,
            subscription: this.subscription,
            contacts: this.contacts,
            createdAt: this.createdAt
        },
        token: this.token
    };
    return returnObject;
};

UserSchema.methods.getLimitedPublicInfo = function (): PublicUserInfo {
    const returnObject = {
        _id: this._id,
        email: this.email,
        subscription: this.subscription
    };
    return returnObject;
};

//sipher the password and save to the field 'password'
UserSchema.pre("save", function (next: Function) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const user = this;

    if (
        (user.password && user.isModified("password")) ||
        (user.password && user.isNew)
    ) {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) return next(err);

            bcrypt.hash(user.password, salt, (err, hash) => {
                if (err) return next(err);
                user.password = hash;
                next();
            });
        });
    } else return next();

});

//Decrypt and Compare the password
UserSchema.methods.validatePassword = function (password: string): boolean {
    const compare: boolean = bcrypt.compareSync(password, this.password);
    return compare;
};

//createToken
UserSchema.methods.getJWT = function (): string {
    const preToken = jwt.sign(
        {
            id: this._id
        },
        config.secretJwtKey,
        {
            expiresIn: 5000
        }
    );

    const token = preToken;

    this.token = token;
    this.save();
    return token;
};