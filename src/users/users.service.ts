import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserErrMsg, UserSccssMsg } from './enums/msg.enum'
import { Model } from 'mongoose';
import { User, UserAfterLogin, UserInfoInArrs, PublicUserInfo, RespondObject } from './interfaces/user.interface';
import CreateUserDto from './dto/create-user.dto';



@Injectable()
export class UserService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async createUser(userDto: CreateUserDto): Promise<Model<UserAfterLogin>> {
        const createdUser = await new this.userModel(userDto).save();
        createdUser.getJWT();
        return createdUser.getPublicFields();
    }

    async getAllUsers(): Model<UserInfoInArrs[]> {
        const users = await this.userModel.find().select({ _id: 1, email: 1, subscription: 1, createdAt: 1 }).exec();
        if (!users) throw new HttpException(UserErrMsg.SERVICE_UNAVAILABLE, HttpStatus.SERVICE_UNAVAILABLE)
        return users as UserInfoInArrs[];
    }

    async findByEmail(email: string): Model<PublicUserInfo> {
        const user = await this.userModel.findOne({ email: email });
        if (!user) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        return user.getLimitedPublicInfo() as PublicUserInfo;
    }

    async checkExistingUser(email: string): Promise<UserAfterLogin> {
        const user = await this.userModel.findOne({ email: email });
        if (!user) return null;
        return user.getPublicFields() as UserAfterLogin;
    }

    async checkById(id: string): Promise<boolean> {
        const user = await this.userModel.findById(id);
        return user ? true : null;
    }

    async getUserById(usersId: string): Promise<Model<PublicUserInfo>> {
        const user = await this.userModel.findById(usersId);
        if (!user) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        return user.getLimitedPublicInfo() as PublicUserInfo;
    }
    async sortUserBySubscr(sub: string): Promise<UserInfoInArrs[]> {
        const users = await this.userModel.find({ subscription: sub }).select({ email: 1, subscription: 1, _id: 1 });
        return users as UserInfoInArrs[];
    }

    async editUser(usersId: string, newFields: { email?: string, subscription?: string }): Model<UserAfterLogin> {
        if (usersId.length !== 24) throw new HttpException(UserErrMsg.INVALID_ID, HttpStatus.BAD_REQUEST);
        const foundUser = await this.getUserById(usersId);
        if (!foundUser) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);

        const updatedUser = await this.userModel.findByIdAndUpdate(usersId, { $set: newFields }, { new: true });
        return updatedUser.getPublicFields() as UserAfterLogin;
    }

    async addNewContact(usersId: string, contactId: string): Promise<any> {
        if (usersId.length !== 24) throw new HttpException(UserErrMsg.INVALID_ID, HttpStatus.BAD_REQUEST);
        const updatedAuthorUser = await this.userModel.findOneAndUpdate(
            { _id: usersId },
            { $push: { contacts: contactId } },
            { new: true, upsert: true }
        );
        if (!updatedAuthorUser) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);

        return updatedAuthorUser;
    }

    async changePassword(usersId: string, email: string, oldPass: string, newPassword: string): Promise<RespondObject> {
        if (usersId.length !== 24) throw new HttpException(UserErrMsg.INVALID_ID, HttpStatus.BAD_REQUEST);
        const isOldPassValid = await this.validatePassword(email, oldPass);
        if (!isOldPassValid) throw new HttpException(UserErrMsg.WRONG_OLD_PASSWORD, HttpStatus.BAD_REQUEST);

        const updatedUser = await this.userModel.findById(usersId, (err, doc) => {
            if (err) return false;
            doc.password = newPassword;
            doc.save();
        })
        if (!updatedUser) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);

        return {
            message: UserSccssMsg.CHANGED_PASSWORD,
            user: updatedUser.getPublicFields()
        };
    }

    async deleteUser(usersId: string): Promise<RespondObject> {
        const foundUser = await this.getUserById(usersId);
        if (!foundUser) throw new HttpException(UserErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        const deletedUser = await this.userModel.findOneAndDelete({ _id: usersId });

        return {
            message: UserSccssMsg.DELETED,
            user: deletedUser.getPublicFields()
        };
    }

    async validatePassword(email: string, password: string): Promise<boolean> {
        const found = await this.userModel.findOne({ email: email });
        const isValidPassword: boolean = await found.validatePassword(password);

        return isValidPassword;
    }

    async generateNewToken(email: string): Promise<void> {
        const user = await this.userModel.findOne({ email: email });
        await user.getJWT();
    }

}