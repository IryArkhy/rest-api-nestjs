import { Module } from '@nestjs/common';
import { UserController } from './users.controller';
import { UserService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';

import { UserSchema } from './model/user.model';
import AuthModule from '../authentication/auth.module';



@Module({
    imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
    //If you also want to use the models in another module, add MongooseModule to the exports section of UserModule and import UserModule in the other module.
})
export default class UsersModule { }