import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import config from './config';

import { ContactModule } from './contacts/contact.module';
import AuthModule from './authentication/auth.module';
import UsersModule from './users/users.module';

const { mongoDbUri, dbOptions } = config;

@Module({
  imports: [MongooseModule.forRoot(mongoDbUri, dbOptions), UsersModule, ContactModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

