import { Module } from '@nestjs/common';
import { ContactController } from './contact.controller';
import { ContactService } from './contact.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ContactSchema } from './model/contact.model'
import UsersModule from '../users/users.module';



@Module({
    imports: [UsersModule, MongooseModule.forFeature([{ name: "Contact", schema: ContactSchema }])],
    controllers: [ContactController],
    providers: [ContactService],
})
export class ContactModule { }