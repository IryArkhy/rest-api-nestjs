import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserService } from '../users/users.service';
import { Contact, PaginatedContacts, RespondObject, ContactLookup, PublicContactInfo } from './interfaces/contacts.interface';
import { ContactErrMsg, ContactSccssMsg } from './enums/msgs.enums';



@Injectable()
export class ContactService {
    constructor(
        @InjectModel('Contact') private readonly contactModel: Model<Contact>,
        private userService: UserService
    ) { }

    async getAllContacts(): Promise<Contact[]> {
        const contacts = await this.contactModel.find().exec();
        if (!contacts) throw new HttpException(ContactErrMsg.SERVICE_UNAVAILABLE, HttpStatus.SERVICE_UNAVAILABLE)
        return contacts as Contact[];
    }

    async getPaginatedContacts(limit: number, page: number): Promise<PaginatedContacts> {
        const optionsPagination = {
            page: page || 1,
            limit: limit || 2,
            select: { name: 1, phone: 1, _id: 0 }
        };

        const contacts = await this.contactModel.paginate({}, optionsPagination);
        if (!contacts) throw new HttpException(ContactErrMsg.SERVICE_UNAVAILABLE, HttpStatus.SERVICE_UNAVAILABLE);
        return contacts;
    }

    async getContactById(contactId: string): Model<Contact> {
        const contact = await this.contactModel.findById(contactId);
        if (!contact) throw new HttpException(ContactErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        return contact as Contact;
    }

    async getContactByOwner(ownerId: string): Promise<PublicContactInfo[]> {
        const contacts = await this.contactModel.find({ owner: ownerId }).populate("contacts").select({ name: 1, email: 1 });
        if (!contacts) throw new HttpException(ContactErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        return contacts;
    }

    async createContact(name: string, email: string, phone: string, owner: string): Model<Contact> {
        //aggregate owner contacts
        const ownerContacts: ContactLookup[] = await this.contactModel.find({ owner: owner }).populate("contacts").select({ email: 1, phone: 1, _id: 0 })

        //check duplication
        const duplication = ownerContacts.reduce((acc, contact) => {
            if (contact.phone === phone) acc.phone = true;
            if (contact.email === email) acc.email = true;
            return acc;
        }, { phone: false, email: false });

        if (duplication.phone || duplication.email) throw new HttpException(ContactErrMsg.DUPLICATION, HttpStatus.BAD_REQUEST);

        //save new contact
        const newContact = new this.contactModel({ name, email, phone, owner });
        const contact = await newContact.save();
        //update Owner
        await this.userService.addNewContact(owner, contact._id);
        if (!contact) throw new HttpException(ContactErrMsg.SERVICE_UNAVAILABLE, HttpStatus.SERVICE_UNAVAILABLE)
        return contact as Contact;
    }

    async editContact(contactId: string, newFields: {}): Model<Contact> {
        if (contactId.length !== 24) throw new HttpException(ContactErrMsg.INVALID_ID, HttpStatus.BAD_REQUEST);
        const foundContact = await this.getContactById(contactId);
        if (!foundContact) throw new HttpException(ContactErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        const updatedContact = this.contactModel.findByIdAndUpdate(contactId, { $set: newFields }, { new: true });
        return updatedContact;
    }

    async deleteContact(contactId: string): Promise<RespondObject> {
        const foundContact = await this.getContactById(contactId);
        if (!foundContact) throw new HttpException(ContactErrMsg.NOT_FOUND, HttpStatus.NOT_FOUND);
        const deletedContact = await this.contactModel.findOneAndDelete({ _id: contactId });

        return {
            message: ContactSccssMsg.DELETED,
            contact: deletedContact
        };
    }
}