import * as dotenv from 'dotenv';
dotenv.config();
const isDev = process.env.NODE_ENV === "development";
const appURL = isDev ? process.env.URL_DEV : process.env.URL_PROD;
export default {
    mongoDbUri: process.env.MONGO_DB_URI || "mongodb://localhost:27027/app",
    dbOptions: {
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
        useNewUrlParser: true
    },
    port: process.env.PORT || 5000,
    mode: process.env.NODE_ENV || "development",
    appUrl: isDev ? `${appURL}:${this.port}` : appURL,
    secretJwtKey: process.env.JWT_SECRET_KEY
}
