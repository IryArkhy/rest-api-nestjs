export enum ContactErrMsg {
    SERVICE_UNAVAILABLE = "Something wrong with DB. Try later",
    NOT_FOUND = "The contact is not found.",
    INVALID_ID = "Bad request. Invalid ID.",
    DUPLICATION = "This person already has a contact with such phone or email."
}

export enum ContactSccssMsg {
    DELETED = "Deleted successfully"
}