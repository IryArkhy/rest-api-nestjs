export interface Contact {
    _id: string,
    name: string,
    email: string,
    phone: string,
    owner: string,
    __v: number,
}

export interface ContactInArrs {
    name: string,
    phone: string,
    owner: string,
}

export interface ContactLookup {
    email: string,
    phone: string
}

export interface PublicContactInfo {
    _id: string,
    email: string,
    phone: string
}

export interface RespondObject {
    message: string,
    contact: Contact,
}

export interface PaginatedContacts {
    docs: ContactInArrs[],
    totalDocs: number,
    limit: number,
    totalPages: number,
    page: number,
    pagingCounter: number,
    hasPrevPage: boolean,
    hasNextPage: boolean,
    prevPage: null,
    nextPage: number
}