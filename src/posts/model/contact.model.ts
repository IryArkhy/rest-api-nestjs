import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

// const paginate: Function = mongoosePaginate;

const validateEmail = function (email: string): boolean {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

export const ContactSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 40,
        minlength: 3
    },
    email: {
        type: String,
        index: true,
        trim: true,
        unique: false,
        required: true,
        validate: [validateEmail, 'Please fill a valid email address'],
    },
    phone: {
        type: String,
        trim: true,
        required: true,
    },
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: "Users"
    }
});
ContactSchema.plugin(mongoosePaginate);
