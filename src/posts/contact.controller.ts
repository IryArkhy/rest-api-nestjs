import { Controller, Get, Post, Patch, Delete, Body, Param, Query } from '@nestjs/common';
import { Contact, PaginatedContacts, RespondObject, PublicContactInfo } from './interfaces/contacts.interface'
import { ContactService } from './contact.service';

import { UserService } from '../users/users.service';



@Controller('api/contacts')
export class ContactController {
    constructor(private readonly contactService: ContactService) { }

    @Get()
    getAllContacts(): Promise<Contact[]> {
        return this.contactService.getAllContacts();
    }
    @Get('paginate')
    getAllContactsPagination(@Query('limit') limit: number, page: number): Promise<PaginatedContacts> {
        return this.contactService.getPaginatedContacts(limit, page);
    }

    @Get(':contactId')
    getContactById(@Param('contactId') contactId: string): Contact {
        return this.contactService.getContactById(contactId);
    }

    @Get('owner/:ownerId')
    async getContactsByOwner(@Param('ownerId') ownerId: string): Promise<PublicContactInfo[]> {
        return this.contactService.getContactByOwner(ownerId);
    }

    @Post()
    createContact(
        @Body('name') contactName: string,
        @Body('email') contactEmail: string,
        @Body('phone') contactPhone: string,
        @Body('owner') owner: string,
    ): Promise<Contact> {
        return this.contactService.createContact(contactName, contactEmail, contactPhone, owner);

    }

    @Patch(':contactId')
    editContact(@Param('contactId') contactId: string, @Body() newFields: {}): Contact {
        return this.contactService.editContact(contactId, { ...newFields })
    }

    @Delete(':contactId')
    deleteContact(@Param('contactId') contactId: string): Promise<RespondObject> {
        return this.contactService.deleteContact(contactId)
    }
}