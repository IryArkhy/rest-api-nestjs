🌐 **Introduction** 
______________________
RESTful API for a small application designed to store user contacts. API enables user authentification.


📕 The full documentation on API [here](https://documenter.getpostman.com/view/8784809/Szf549pv?version=latest).

🎀 **Overview**
- API is written with Nestjs.
- MongoDB.
- No CORS
- The very first request may take a while. 
Authentication
- JWT Strategy. Bearer token may be required.

🛑 **Error Codes**
```
NOT_FOUND ----------------- 404
BAD_REQUEST --------------- 400
SERVICE_UNAVAILABLE ------- 503
UNAUTHORIZED -------------- 401
CONFLICT ------------------ 409
INTERNAL_SERVER_ERROR ----- 500
```